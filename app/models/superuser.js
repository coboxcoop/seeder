const Superuser = require('@coboxcoop/superusers')
const { assign } = Object

module.exports = function (storage, identity, opts) {
  return function (params) {
    return Superuser(
      storage,
      params.address,
      identity,
      assign(params, opts)
    )
  }
}
