const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const Replicator = require('@coboxcoop/replicator')
const { assign } = Object

module.exports = function (storage, identity, opts) {
  return function (params) {
    return Replicator(
      storage,
      params.address,
      identity,
      assign(params, opts)
    )
  }
}
