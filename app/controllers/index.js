const CommandsController = require('./commands')
const ReplicatorsController = require('./replicators')
const KeysController = require('./keys')
const ProfileController = require('./profile')
const SystemController = require('./system')
const InvitesController = require('./invites')

class ApplicationController {
  constructor (api) {
    this.api = api
    this.commands = new CommandsController(api)
    this.replicators = new ReplicatorsController(api)
    this.keys = new KeysController(api)
    this.profile = new ProfileController(api)
    this.system = new SystemController(api)
    this.invites = new InvitesController(api)
  }
}

module.exports = ApplicationController
