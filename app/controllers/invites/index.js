const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const Invite = require('@coboxcoop/key-exchange')
const { loadKey } = require('@coboxcoop/keys')
const pick = require('lodash.pick')
const SpaceDecorator = require('../../../lib/decorators/space')
const { hex } = require('../../../util')
const zero = require('sodium-native').sodium_memzero
const PeerInvite = require('@coboxcoop/schemas').encodings.peer.invite

const { assign } = Object

class InvitesController {
  constructor (api) {
    this.api = api
  }

  // -------------------- Controller Actions -------------------- //

  // POST /api/invites
  async create (params, opts = {}) {
    let code = Invite.create(inviteParams(this.api.superuser, params))

    let msg = {
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: { publicKey: hex(params.publicKey) }
    }

    // this informs other superuser members that a new peer has been invited
    await this.api.superuser.log.publish(msg, { valueEncoding: PeerInvite })

    msg.content.code = hex(code)

    return msg
  }
}

function inviteParams (seeder, params) {
  var encryptionKey = loadKey(seeder.storage, 'encryption_key')
  var seederParams = assign(SpaceDecorator(seeder).toJSON(), params)
  var picked = pick(seederParams, ['publicKey', 'address', 'name'])
  var p = assign({ encryptionKey: hex(encryptionKey) }, picked)
  zero(encryptionKey)
  return p
}

module.exports = InvitesController
