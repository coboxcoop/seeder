const through = require('through2')
const { validators } = require('@coboxcoop/schemas')
const set = require('lodash.set')
const sortBy = require('lodash.sortby')
const transform = require('lodash.transform')
const collect = require('collect-stream')
const crypto = require('@coboxcoop/crypto')
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const { assign } = Object
const pick = require('lodash.pick')

const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

const NotFoundError = require('../../../../lib/errors/not-found')
const filter = require('../../../helpers/filter')
const resourceParams = require('../../../helpers/resource-params')
const genId = require('../../../helpers/gen-id')
const { hex } = require('../../../../util')

class ReplicateCommandsController {
  constructor (api) {
    this.api = api
    this.state = api.state.commands.replicates
    // {
    //   [address]: {
    //     [commandId]: {
    //       name,
    //       address,
    //       timestamp,
    //       command
    //     }
    //   }
    // }
  }

  live (params, opts = {}) {
    return this.api.superuser.log
      .read({ query: query(), live: true, old: false })
      .pipe(filter(isValid))
      .pipe(reduce(this.state))
  }

  async index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const commands = this.api.superuser.log
        .read({ query: query() })
        .pipe(filter(isValid))
        .pipe(reduce())

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  async command (params, opts = {}) {
    await this.index()
    reduceState(this.state)

    this.live().on('data', async (msg) => {
      const records = reduceState(this.state)
      if (!records) return false
      let actions = await Promise.all(records.map(this.findOrCreateReplicator.bind(this)))

      return await Promise.all(actions.map(async (action) => {
        try {
          await this.execute(action.command, action.params)
        } catch (err) {
          debug({ msg: 'failed to execute command', command: action.command, params, err })
          throw err
        }
      }))
    })
  }

  // --------------------- Helpers ---------------------- //

  async findOrCreateReplicator (record) {
    const params = resourceParams(record)
    const details = record.commands[record.commands.length - 1]
    const command = details.command

    let replicator

    try {
      replicator = await this.api.controllers.replicators.show(params)
      return { params, command }
    } catch (err) {
      if (!err.notFound) throw err
      replicator = await this.api.controllers.replicators.create(params)
      return { params, command }
    }
  }

  async execute (command, params) {
    var mappings = this._mappings()
    var fn = mappings[command]
    if (fn) return await fn(params, {})
    else throw new NotFoundError('invalid command')
  }

  _mappings () {
    return {
      [REPLICATE]: (params, opts) => this.api.controllers.replicators.connections.create({}, params),
      [UNREPLICATE]: this.api.controllers.replicators.connections.destroy.bind(this.api.controllers.replicators.connections)
    }
  }
}

function query (params = {}, opts = {}) {
  const $map = opts.$map || {}
  const $reduce = opts.$reduce || {}
  const value = Object.assign(params, { type: { $in: [REPLICATE, UNREPLICATE] },  timestamp: { $gt: 0 } })
  return [{ $filter: { value }, $map, $reduce }]
}


function isValid (msg) {
  return isReplicate(msg.value) || isUnreplicate(msg.value)
}

function reduce (state) {
  return through.obj(function (msg, enc, next) {
    const { timestamp, type } = msg.value
    const { name, address } = msg.value.content
    const details = { name, address, command: type, timestamp }
    const id = hex(genId(msg))
    set(state, [address, id], details)
    this.push(details)
    next()
  })
}

function reduceState (state) {
  return transform(state, (acc, records, address) => {
    let values = Object.values(records)
    let last = values[values.length-1]
    acc.push({
      address,
      name: last.name,
      commands: transform(records, (acc, details, id) => {
        acc.push(assign({ id }, {
          timestamp: details.timestamp,
          command: details.command
        }))
      }, [])
    })
  }, [])
}

module.exports = ReplicateCommandsController
