const AnnouncementCommandsController = require('./announcements')
const ReplicateCommandsController = require('./replicates')

class CommandsController {
  constructor (api) {
    this.announcements = new AnnouncementCommandsController(api)
    this.replicates = new ReplicateCommandsController(api)
  }
}

module.exports = CommandsController
