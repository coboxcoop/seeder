const through = require('through2')
const { encodings, validators } = require('@coboxcoop/schemas')
const { loadKey } = require('@coboxcoop/keys')
const set = require('lodash.set')
const sortBy = require('lodash.sortby')
const transform = require('lodash.transform')
const collect = require('collect-stream')
const { assign } = Object
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder:announcements')

const NotFoundError = require('../../../../lib/errors/not-found')
const Announcement = require('../../../../lib/announcement')

const isAnnounce = validators.command.announce
const isHide = validators.command.hide

const filter = require('../../../helpers/filter')
const genId = require('../../../helpers/gen-id')
const { hex } = require('../../../../util')

const ANNOUNCE = 'command/announce'
const HIDE = 'command/hide'

class AnnouncementCommandsController {
  constructor (api) {
    this.api = api
    this.state = api.state.commands.announcements
    // {
    //   [commandId]: {
    //     timestamp,
    //     command
    //   }
    // }
    this._mappings = {
      [ANNOUNCE]: this.create.bind(this),
      [HIDE]: this.destroy.bind(this)
    }
  }

  live (params, opts = {}) {
    return this.api.superuser.log
      .read({ query: query(), live: true, old: false })
      .pipe(filter(isValid))
      .pipe(reduce())
  }

  async index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const commands = this.api.superuser.log
        .read({ query: query() })
        .pipe(filter(isValid))
        .pipe(reduce())

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  async command (params, opts) {
    await this.index()
    const commands = reduceState(this.state)
    if (!commands[commands.length - 1] && this.api.settings.announce) await this.create()

    this.live().on('data', async (msg) => {
      if (msg.sync) return
      try { return await this.execute(msg.command, msg) }
      catch (err) {
        debug({ msg: 'failed to execute command', command: msg.command, value: msg, err })
        return null
      }
    })
  }

  create (params, opts) {
    const buildPayload = () => ({
      type: 'seeders/broadcast',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: {
        address: hex(this.api.superuser.address),
        encryptionKey: hex(loadKey(this.api.superuser.storage, 'encryption_key')),
        broadcasting: true
      }
    })

    return this.announcement.start(buildPayload)
  }

  destroy (params, opts) {
    return this.announcement.stop()
  }

  async execute (command, params) {
    var fn = this._mappings[command]
    if (fn) return await fn(params, opts = {})
    else throw new NotFoundError('invalid command')
  }
}

function query (params = {}, opts = {}) {
  const $map = opts.$map || {}
  const $reduce = opts.$reduce || {}
  const value = Object.assign(params, { type: { $in: [ANNOUNCE, HIDE] },  timestamp: { $gt: 0 } })
  return [{ $filter: { value }, $map, $reduce }]
}

function isValid (msg) {
  return isAnnounce(msg.value) || isHide(msg.value)
}

function reduce (state) {
  return through.obj(function (msg, enc, next) {
    const { timestamp, type: command } = msg.value
    const details = { command, timestamp }
    const id = hex(genId(msg))
    set(state, [id], details)
    this.push(details)
    next()
  })
}

function reduceState (state) {
  return transform(state, (acc, record, id) => {
    acc.push(assign({ id }, record))
  }, [])
}

module.exports = AnnouncementCommandsController
