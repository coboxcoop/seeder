const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const { loadKey } = require('@coboxcoop/keys')
const SpaceDecorator = require('../../../lib/decorators/space')
const { hex } = require('../../../util')

const PARENT_KEY = 'parent_key'

class KeysController {
  constructor (api) {
    this.api = api
  }

  // GET /api/keys
  async index (params, opts = {}) {
    let replicators = await this.api.replicators.store.all()

    return {
      parentKey: hex(loadKey(this.api.settings.storage, PARENT_KEY)),
      admin: SpaceDecorator(this.api.superuser).export(),
      replicators: replicators
        .map(SpaceDecorator)
        .map((decorator) => decorator.export())
    }
  }

  // POST /api/keys
  async create (params, opts = {}) {
    throw new Error('route not yet implemented')
  }
}

module.exports = KeysController
