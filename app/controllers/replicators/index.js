const debug = require('@coboxcoop/logger')('@coboxcoop/seeder:replicators')
const assert = require('assert')
const resourceParams = require('../../helpers/resource-params')
const SpaceDecorator = require('../../../lib/decorators/space')
const ConnectionsController = require('./connections')
const { encodings, validators } = require('@coboxcoop/schemas')

const Replicate = encodings.command.replicate
const Unreplicate = encodings.command.unreplicate
const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate
const { assign } = Object

const { hex } = require('../../../util')

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

class ReplicatorsController {
  constructor (api) {
    this.api = api
    this.replicators = api.replicators
    this.connections = new ConnectionsController(api, this)
  }

  // GET /api/replicators
  async index (params, opts = {}) {
    let replicators = await this.replicators.store.all()

    if (!opts.decorate) return replicators

    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/replicators/:id
  async show (params, opts = {}) {
    let replicator = await this.replicators.store.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    if (!opts.decorate) return replicator

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators
  async create (params, opts = {}) {
    let replicator = await this.replicators.store.create(resourceParams(params))

    assert(replicator, 'failed to create')

    let msg = {
      type: REPLICATE,
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: replicator.attributes
    }

    assert(isReplicate(msg), 'invalid parameters')

    await this.api.superuser.log.publish(msg, { valueEncoding: Replicate })

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators/:id
  async destroy (params, opts = {}) {
    let replicator = await this.repository.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    try {
      await replicator.destroy()
      await this.repository.remove(resourceParams(params))
    } catch (err) {
      throw err
    }

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }
}

module.exports = ReplicatorsController
