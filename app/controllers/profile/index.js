const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const crypto = require('@coboxcoop/crypto')
const ProfileDecorator = require('../../../lib/decorators/profile')
const SpaceDecorator = require('../../../lib/decorators/space')
const { hex } = require('../../../util')
const { assign } = Object

class ProfileController {
  constructor (api) {
    this.api = api
  }

  show (params, opts) {
    return assign(
      ProfileDecorator(this.api.profile).toJSON(),
      { address: hex(this.api.superuser.address) }
    )
  }
}

module.exports = ProfileController
