const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const pick = require('lodash.pick')

class SystemController {
  constructor (api) {
    this.api = api
  }

  async show (params, opts) {
    const system = systemParams(this.api.system)
    return system
  }

  async routes (params, opts) {
    const router = params.router
    return []
      .concat
      .apply([], router.stack.map((layer) => this._recurseRoutes(layer)))
      .filter(Boolean)
  }

  _recurseRoutes (layer) {
    if (!layer.route) {
      var match = layer.regexp.toString().match(/\/\^\\(\/\w+)/)
      if (!match) return
      var path = match[1]

      var stack = layer.handle.stack
      var nestedRoutes = stack.map((l) => {
        var rs = this._recurseRoutes(l)
          .map((route) => {
            route.path = path.concat(route.path)
            return route
          })

        return rs
      })
      var rs = [].concat.apply([], nestedRoutes)
      return rs
    } else {
      var path = layer.route.path
      if (!path) return
      var rs = layer.route.stack.reduce((acc, l) => {
        acc.push({ path, method: l.method })
        return acc
      }, [])
      return rs
    }
  }
}

function systemParams (params) {
  return pick(params, [
    'name',
    'description',
    'version',
    'author',
    'license',
    'bugs'
  ])
}

module.exports = SystemController
