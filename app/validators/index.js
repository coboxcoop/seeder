const ReplicatorsValidator = require('./replicators')

const Validators = module.exports = (api) => ({
  replicators: ReplicatorsValidator(api)
})
