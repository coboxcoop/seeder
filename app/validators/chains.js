const assert = require('assert')
const crypto = require('@coboxcoop/crypto')
const { body } = require('express-validator')
const { definitions } = require('@coboxcoop/schemas')

const isRequired = `is required`
const isKey = `must be a 32 byte key`
const isName = `must be below 32 characters`
const isIncorrectFormat = 'must be a 272+ character hex code'

function encryptionKeyChain () {
  return body('encryptionKey')
    .not()
    .isEmpty()
    .withMessage(isRequired)
    .bail()
    .custom((encryptionKey) => crypto.isKey(encryptionKey))
    .withMessage(isKey)
    .bail()
}

function publicKeyChain () {
  return body('publicKey')
    .not()
    .isEmpty()
    .withMessage(isRequired)
    .bail()
    .custom((publicKey) => crypto.isKey(publicKey))
    .withMessage(isKey)
    .bail()
}

function nestedCommandsChain () {
  return body('commands')
    .optional()
    .isArray()
    .withMessage('must be an array')
    .bail()
    .custom((commands) => commands.filter((cmd) => cmd.action).length === commands.length)
    .withMessage('must be a valid action')
    .bail()
}

function SpaceByNameChain (store) {
  const throwWhenSpacePresent = ThrowWhenEntryPresent(store)

  return function spaceByNameChain () {
    return body('name')
      .not()
      .isEmpty()
      .withMessage(isRequired)
      .bail()
      .custom((name) => !crypto.isKey(name))
      .withMessage(isName)
      .bail()
      .custom(async (name, { req }) => await throwWhenSpacePresent({ name }))
      .bail()
  }
}

function SpaceByAddressChain (store) {
  const throwWhenSpacePresent = ThrowWhenEntryPresent(store)

  return function spaceByAddressChain () {
    return body('address')
      .not()
      .isEmpty()
      .withMessage(isRequired)
      .bail()
      .custom((address) => crypto.isKey(address))
      .withMessage(isKey)
      .bail()
      .custom(async (address, { req }) => await throwWhenSpacePresent({ address }))

  }
}

function codeChain () {
  return body('code')
    .not().isEmpty()
    .withMessage(isRequired)
    .bail()
    .matches(definitions.inviteCode.pattern)
    .withMessage(isIncorrectFormat)
    .bail()
}

module.exports = {
  encryptionKeyChain,
  publicKeyChain,
  nestedCommandsChain,
  codeChain,
  SpaceByNameChain,
  SpaceByAddressChain
}

function ThrowWhenEntryPresent (store) {
  return async function throwWhenEntryPresent (params) {
    try {
      var entry = await store.findBy(params)
      assert(!entry, `already exists`)
    } catch (err) {
      if (err.notFound) return true
      throw err
    }
  }
}
