const express = require('express')
const toHTTP = require('../middleware/to-http')
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')

const ApplicationController = require('../controllers')
const ApplicationValidator = require('../validators')

module.exports = function Routes (api) {
  const router = express.Router()

  const app = {
    controllers: new ApplicationController(api),
    validators: ApplicationValidator(api)
  }

  api.controllers = app.controllers

  router.use('/commands', require('./commands')(app))
  router.use('/replicators', require('./replicators')(app))
  router.use('/keys', require('./keys')(app))
  router.use('/profile', require('./profile')(app))
  router.use('/system', require('./system')(app))
  router.use('/invites', require('./invites')(app))

  router.get('/system/routes', (req, res) => {
    let routes = app.controllers.system.routes({ router })

    return res
      .status(200)
      .json(routes)
  })

  router.get('/stop', (req, res) => {
    api.gracefulExit()
      .then((done = noop) => {
        res.status(200)
        res.end()
        return process.nextTick(done)
      }).catch((err, done = noop) => {
        debug({ msg: 'failed to exit gracefully', err })
        res.status(500)
        res.end()
        return process.nextTick(done)
      })
  })

  return router
}

