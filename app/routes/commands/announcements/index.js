const express = require('express')
const toHTTP = require('../../../middleware/to-http')

const AnnouncementRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()

  router.post('/', toHTTP(controllers.commands.announcements, 'create'))
  router.delete('/', toHTTP(controllers.commands.announcements, 'destroy'))

  return router
}
