const express = require('express')
const toHTTP = require('../../middleware/to-http')

const InviteRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()

  router.post('/', toHTTP(controllers.invites, 'create'))

  return router
}
