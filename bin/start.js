const constants = require('@coboxcoop/constants')
const deepEqual = require('deep-equal')
const Ora = require('ora')
const prompts = require('prompts')
const path = require('path')
const config = require('@coboxcoop/config')
const os = require('os')
const options = require('./lib/options')
const { merge } = require('lodash')
const { getDefaultOpts } = require('@coboxcoop/seeder-cli/bin/lib/util')
const { untilde, printAppInfo } = require('../util')
const launch = require('../launch')

exports.command = ['start [options]', 'up']
exports.desc = 'start cobox'
exports.builder = {}
exports.handler = setup

const SEEDERRC_PATH = path.join(os.homedir(), '.coboxseederrc')
const DEFAULT_SPACE_SUFFIX = "'s space"

if (require.main === module) return setup(require('yargs').options(require('./lib/options')).argv)

async function setup (argv) {
  let configPath = argv.config || SEEDERRC_PATH

  const spinner = new Ora({
    text: `Loading configuration from ${configPath}...`
  })

  spinner.start()

  // load maybe existing config from argv.config or default RC path
  let cfg = config.load(configPath)
  // all changes will be assigned to the copy
  // existing config takes priority over defaults
  let newCfg = Object.assign({}, getDefaultOpts(), cfg)
  if (!cfg.storage) spinner.fail('No configuration found. Lets make one!')
  else spinner.succeed()

  let nickname = cfg.nickname
  if (!nickname) {
    const questions = [
      {
        type: 'text',
        name: 'nickname',
        message: 'How do you want your seeder to be known?',
        initial: 'AnonymousElephant'
      }
    ]

    const response = await prompts(questions)
    newCfg.nickname = response.nickname
    process.stdout.write(`Hi ${newCfg.nickname}, welcome...\n`)
  } else {
    process.stdout.write(`Hi ${newCfg.nickname}, welcome back!\n`)
  }

  // ensure defaults are set with a lower priority than previous config
  cfg = Object.assign({}, constants.seederDefaults, cfg)
  // check if any values have changed, if so, overwrite old RC file
  if (!deepEqual(newCfg, cfg)) {
    spinner.text = `Saving configuration to ${configPath}`
    spinner.start()
    config.save(configPath, newCfg)
    spinner.succeed()
    cfg = newCfg
  }

  // prioritise args, but use other cfg values
  opts = merge({}, cfg, argv)
  // ensure we have a full path set, for printAppInfo display
  opts.storage = path.resolve(untilde(opts.storage))

  spinner.text = `Starting CoBox Seeder...`
  spinner.start()
  await startServer()
  spinner.succeed()
  printAppInfo(opts)

  function exit (err) {
    if (spinner) spinner.fail(err.message)
    console.error(err || 'setup quit')
    process.exit(1)
  }

  function startServer () {
    return new Promise((resolve, reject) => {
      opts.quiet = true
      launch(opts, (err) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }
}
