const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { handleAjaxErrors, getDefaultOpts } = require('@coboxcoop/seeder-cli/bin/lib/util')

exports.command = 'stop [options]'
exports.desc = 'stop the seeder'
exports.builder = getDefaultOpts()
exports.handler = stop

async function stop (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    await client.get('stop')
    console.log('cobox-seeder closed successfully')
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
