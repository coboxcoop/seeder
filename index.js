const Corestore = require('corestore')
const crypto = require('@coboxcoop/crypto')
const express = require('express')
const constants = require('@coboxcoop/constants')
const bodyParser = require('body-parser')
const mkdirp = require('mkdirp')
const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const { loadKey, saveKey } = require('@coboxcoop/keys')
const sodium = require('sodium-native')
const assert = require('assert')
const cors = require('cors')
const capture = require('capture-exit')
const start = require('./lib/start')
const close = require('./lib/close')
const Routes = require('./app/routes')
const SuperuserStore = require('./lib/superusers')
const ReplicatorStore = require('./lib/replicators')
const Announcement = require('./lib/announcement')
const { keyIds } = constants
const { hex, untilde } = require('./util')
const { assign } = Object
const { requestLogger } = require('@coboxcoop/logger')
const CoboxNetworker = require('@coboxcoop/replicator/network')

const VERSION = 'v1'
const PARENT_KEY = 'parent_key'

capture.captureExit()

function Application (opts = {}) {
  const _id = hex(crypto.randomBytes(2))
  const storage = opts.storage && path.join(untilde(opts.storage), VERSION) || path.join(constants.seederStorage, VERSION)
  const udpPort = opts.udpPort || 8999
  const port = opts.port || 9111
  const settings = { storage, port, udpPort }

  mkdirp.sync(path.join(storage))

  const app = express()

  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(cors())
  app.use(requestLogger('@coboxcoop/seeder:request'))
  app.use(require('./app/middleware/log-request'))

  const parentKey = loadKey(storage, PARENT_KEY) || crypto.masterKey()
  saveKey(storage, PARENT_KEY, parentKey)
  const identity = crypto.deriveBoxKeyPair(parentKey, keyIds.identity)
  sodium.sodium_memzero(parentKey)

  const corestore = new Corestore(path.join(storage, 'store'), { masterKey: parentKey })
  const network = new CoboxNetworker(corestore)

  network.listen()

  const superusers = SuperuserStore(storage, { identity, corestore, network })
  const replicators = ReplicatorStore(storage, { identity, corestore, network })
  const announcement = Announcement({ port: udpPort })

  app.gracefulExit = Shutdown(app)

  const api = {
    superusers: { store: superusers },
    replicators: { store: replicators },
    announcement,
    state: defaultState({}),
    settings,
    profile: identity,
    gracefulExit: app.gracefulExit
  }

  app.use('/api', Routes(api))
  app.set('api', api)

  app.start = api.start = start(app, assign(opts, settings))
  app.close = api.close = close(app, assign(opts, settings))

  capture.onExit(api.gracefulExit)

  function Shutdown (app) {
    return function gracefulShutdown () {
      return new Promise((resolve, reject) => {
        app.close((err, done) => {
          if (err) return reject(err)
          return resolve(done)
        })
      })
    }
  }

  return app
}

function defaultState (opts = {}) {
  return {
    commands: {
      announcements: {},
      replicates: {}
    }
  }
}

module.exports = Application
