const { isString } = require('util')
const os = require('os')
const path = require('path')
const chalk = require('chalk')
const { DEFAULT_COLOUR } = require('@coboxcoop/seeder-cli/bin/lib/util')
const { printBanner } = require('@coboxcoop/seeder-cli/util')
const constants = require('@coboxcoop/constants')

function printAppInfo (opts) {
  const {
    hostname = 'localhost',
    port = constants.seederDefaults.port,
    udpPort = constants.seederDefaults.udpPort,
    storage = constants.seederStorage
  } = opts

  printBanner()

  console.log()
  console.log(`listening on ${chalk.hex(DEFAULT_COLOUR)(`http://${hostname}:${port}`)}`)
  console.log(`pairing on ${chalk.hex(DEFAULT_COLOUR)(`http://${hostname}:${udpPort}`)}`)
  console.log(`storage at ${chalk.hex(DEFAULT_COLOUR)(`file://${storage}`)}`)
  console.log()
}

function untilde (str) {
  if (str[0] === '~') return path.join(os.homedir(), str.slice(1))
  return str
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  else return buf
}

function isHexString (str, length) {
  if (!isString(str)) return false
  if (length && (str.length !== length)) return false
  return RegExp('[0-9a-fA-F]+').test(str)
}

function removeEmpty (obj) {
  return Object.keys(obj)
    .filter(k => obj[k] != null)
    .reduce((newObj, k) => {
      return typeof obj[k] === 'object'
        ? { ...newObj, [k]: removeEmpty(obj[k]) }
        : { ...newObj, [k]: obj[k] }
    }, {})
}

function pluck (from, keys) {
  return keys.reduce((acc, key) => {
    if (from[key] !== undefined) acc[key] = from[key]
    return acc
  }, {})
}

function flatten (obj) {
  return Object.keys(obj).reduce((acc, key) => {
    acc.push(argKey(key))
    if (obj[key] !== true) acc.push(obj[key])
    return acc
  }, [])

  function argKey (key) {
    return '--' + key.replace(
      /[\w]([A-Z])/g,
      m => m[0] + '-' + m[1]
    ).toLowerCase()
  }
}

module.exports = {
  isDevelopment: () => process.env.NODE_ENV === 'development',
  isProduction: () => process.env.NODE_ENV === 'production',
  printAppInfo,
  untilde,
  hex,
  isHexString,
  removeEmpty,
  pluck,
  flatten
}
