const fs = require('fs')
const constants = require('@coboxcoop/constants')
const yargs = require('yargs')
const findUp = require('find-up')
const YAML = require('js-yaml')
const config = require('@coboxcoop/config')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:main')

const App = require('./index')
const options = require('./bin/lib/options')
const SEEDERRC = '.coboxseederrc'

const args = yargs
  .config('config', (configPath) => config.load(configPath || SEEDERRC))
  .options(options)
  .argv

if (require.main === module) return run(args)
else module.exports = run

function run (opts) {
  const app = App(opts)

  app.start((err) => {
    if (err) {
      debug({ msg: 'failed to start the app properly', err })
      throw err
    }
  })
}
