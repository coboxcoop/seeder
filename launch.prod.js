const constants = require('@coboxcoop/constants')
const fs = require('fs')
const path = require('path')
const pm2 = require('pm2')
const { pluck, flatten, printAppInfo } = require('./util')

module.exports = function production (opts, cb) {
  const logPath = path.join(path.resolve(opts.storage || constants.seederStorage), 'logs', 'seeder')
  const args = flatten(pluck(opts, ['port', 'hostname', 'storage', 'dev', 'udpPort']))
  const script = require.resolve('./main.js')

  args.push('--env')
  args.push('production')

  fs.mkdirSync(logPath, { recursive: true })

  pm2.connect((err) => {
    if (err) process.exit(2)

    var pm2opts = {
      name: 'cobox-seeder',
      script,
      args,
      env: { NODE_ENV: 'production' },
      error: path.join(logPath, 'error.log'),
      log: path.join(logPath, 'main.log'),
      max_memory_restart: '1000M',
      autorestart: false
    }

    pm2.start(pm2opts, (err, apps) => {
      if (err) throw err
      pm2.disconnect()
      return cb(err, opts)
    })
  })
}
