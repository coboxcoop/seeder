# seeder

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

## Table of Contents

* [About](#about)
* [Installing and Updating](#installing-and-updating)
  * [Install & Update Script](#install-and-update-script)
  * [Troubleshooting on Linux](#troubleshooting-on-linux)
  * [Verify Installation](#verify-installation)
  * [NPM Install](#npm-install)
  * [Git Install](#git-install)
* [Usage](#usage)
* [Contributing](#contributing)
* [License](#license)


## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`seeder` provides an 'always on' encrypted replicator seeder designed to be hosted in your office or home. It exposes an ExpressJS HTTP JSON API, pairing with [CoBox Server](https://gitlab.com/coboxcoop/server) and a Yargs CLI enabling issuing remote commands from a registered and authenticated CoBox Server instance over the [Hyperswarm DHT](https://github.com/hyperswarm/hyperswarm) using the [Hypercore Protocol](https://github.com/mafintosh/hypercore-protocol/).


## Installing and Updating

### Install and Update Script

To install or update @coboxcoop/seeder, you should run the [install script](./download.sh). To do that, you may either download and run the script manually, or use the following cURL or Wget command:

```
curl -o- https://cobox.cloud/releases/cobox-seeder-v1.0.0-alpha.1/download.sh | bash
```

```
wget -qO- https://cobox.cloud/releases/cobox-seeder-v1.0.0-alpha.1/download.sh | bash
```

Running either of the above commands downloads a script and runs it. The script downloads a tarball that contains the release binary required by your operating system with additional assets and unpacks the contents to the install directory.

### Troubleshooting on Linux

On Linux, after running the install script, if you get `cobox-seeder: command not found` or see no feedback from your terminal after you type `command -v cobox-seeder`, simply close your current terminal, open a new terminal, and try verifying again.

### Verify Installation

To verify that CoBox has been installed correctly, do:

```
command -v cobox-seeder
```

### NPM Install

```
npm install -g @coboxcoop/seeder

cobox-seeder
```

### Git Install

Before you get started, ensure you have `git` and `node` installed. We currently release using `v12.16.3` which we recommend using. You can download and install nodejs from your distribution package manager, or [direct from their website](https://nodejs.org/en/download/).

```
# clone the repository
git clone http://gitlab.com/coboxcoop/seeder && cd seeder

npm install

# run the app
node bin start

# use the cli
node bin
```

## Usage

CoBox Seeder can be administrated in two ways.

1. Directly on the seeder that is running it using the CLI
2. By sending commands across the hypercore-protocol from an authenticated CoBox Server instance

### CLI

```
CoBox Seeder 1.0 - a tool for distributed and mutual backup infrastructure

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: cobox-seeder <command> [options]

Commands:
  cobox-seeder invites <command>  manage invites for adding admins to your seeder's
  cobox-seeder keys <command>     manage your seeder's keys
  cobox-seeder seeds <command>    seed your peers' folders
  cobox-seeder whoami             display your seeder's profile
  cobox-seeder start [options]    start cobox                               [aliases: up]
  cobox-seeder stop [options]     stop the seeder

Options:
  -h, --help, --help        Show help                                  [boolean]
  -v, --version, --version  Show version number                        [boolean]

For more information on cobox read the manual: man cobox-seeder
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.

Not enough non-option arguments: got 0, need at least 1
```

### Commands


Once you have installed the seeder you can generate an invite code to add friends (or your own devices) as an admin for this seeder.

```shell
cobox-seeder invites create <cobox-key>
```

Doing so will add them as a superuser to this seeder, with the same powers as the original admin. Doing so will mean the seeder can be remote controlled from within the CoBox App.

Please check our [docs](https://gitlab.com/coboxcoop/docs/-/blob/master/src/become_an_admin.md) for more commands.

# Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
