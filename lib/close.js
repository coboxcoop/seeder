const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const thunky = require('thunky')
const capture = require('capture-exit')

module.exports = function close (app, opts) {
  const api = app.get('api')

  return thunky(function (cb = noop) {
    let pending = 2

    api.superusers.store.close(next)
    api.replicators.store.close(next)

    function next (err) {
      if (err) {
        pending = Infinity
        return cb(err)
      }
      if (!--pending) return cb(null, done)
    }

    function done () {
      capture.offExit(api.gracefulExit)
      debug({ msg: 'shutting down the server' })
      app.server.forceShutdown((err) => {
        debug({ msg: 'closed: server', err })
        capture.releaseExit()
        return process.exit(0)
      })
    }
  })
}
