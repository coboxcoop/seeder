const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const SpaceDecorator = require('./decorators/space')

function logResource (record) {
  var decorator = SpaceDecorator(record)
  var resource = decorator.toJSON({ secure: true })
  debug({ type: record.constructor.name, ...resource })
}

module.exports = {
  logResource
}
