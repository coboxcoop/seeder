const debug = require('@coboxcoop/logger')('@coboxcoop/seeder:start')
const thunky = require('thunky')
const shutdown = require('http-shutdown')
const crypto = require('@coboxcoop/crypto')
const os = require('os')
const { hex } = require('../util')

module.exports = function start (app, opts) {
  const api = app.get('api')

  return thunky(function (cb = noop) {
    console.log("stacking it up, block by block by...")

    let pending = 2

    api.superusers.store.ready(next)
    api.replicators.store.ready(next)

    function next (err) {
      if (err) {
        debug({ msg: 'failed to load repository', err })
        pending = Infinity
        return cb(err)
      }
      if (!--pending) return ready()
    }

    function ready () {
      var superuser = api.superusers.store.first()
      if (superuser) return superuser.ready(() => onready(superuser))
      else initialize((su) => onready(su))

      function initialize (cb) {
        api.superusers.store.create({
          name: opts.nickname || os.hostname(),
          address: hex(crypto.address()),
          encryptionKey: hex(crypto.encryptionKey())
        }).then(cb)
      }

      function onready (superuser) {
        api.superuser = superuser
        superuser.swarm()
        api.controllers.commands.replicates.command().catch(cb).then(done)
        /* TODO: announce by default IF we are shipped inside a box, otherwise don't do that
        api.controllers.commands.announcements.command().catch(cb).then(() => {
        })
        */
      }
    }

    function done () {
      app.set('hostname', opts.hostname)
      app.set('port', opts.port)
      app.set('udpPort', opts.udpPort)
      app.set('storage', opts.storage)

      app.server = app.listen(opts.port, opts.hostname, cb)

      shutdown(app.server)
    }
  })
}
