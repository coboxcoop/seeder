const crypto = require('@coboxcoop/crypto')
const createBroadcastStream = require('broadcast-stream')
const { EventEmitter } = require('events')
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const { hex, isProduction } = require('../util')
const { encodings } = require('@coboxcoop/schemas')
const Broadcast = encodings.seeders.broadcast

const Announcement = module.exports = (opts = {}) => {
  const id = hex(crypto.randomBytes(2))
  const port = opts.port || 8999
  const interval = opts.interval || 1000
  const onconnection = opts.onconnection || noop
  let stream = null
  let end = noop
  let destroy = noop

  return {
    start,
    stop
  }

  function start (buildPayload) {
    if (stream) return debug('already announcing')
    stream = createBroadcastStream(port)
    destroy = setInterval(() => {
      if (stream) stream.write(payload())
    }, interval)

    stream.on('data', onconnection)

    end = () => {
      debug({ id, msg: `announcement shutting down` })
      clearInterval(destroy)
      stream.end()
    }

    return { port, interval }

    function payload () {
      var pl = buildPayload()
      if (!isProduction()) debug({ id, msg: `announcing on port` })
      return Broadcast.encode(pl)
    }
  }

  function stop () {
    if (!end) return debug({ id, msg: 'already stopped' })
    end()
    debug({ id, msg: 'announcement stopped' })
    end = noop
    stream = null
    return {}
  }
}

function noop () {}

module.exports = Announcement
