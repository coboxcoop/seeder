const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const { hex, removeEmpty } = require('../../util')

const ProfileDecorator = module.exports = (profile) => ({
  toJSON: () => removeEmpty({
    publicKey: hex(profile.publicKey)
  })
})
