const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const { hex, removeEmpty } = require('../../util')
const { loadKey } = require('@coboxcoop/keys')

const SpaceDecorator = module.exports = (space) => ({
  toJSON: (opts = {}) => removeEmpty({
    name: space.name,
    address: hex(space.address),
    discoveryKey: space.discoveryKey ? hex(space.discoveryKey) : null,
    encryptionKey: opts.secure ? hex(loadKey(space.storage, 'encryption_key')) : null,
    size: space.bytesUsed(),
    swarming: space.isSwarming()
  }),
  export: () => ({
    name: space.name,
    address: hex(space.address),
    encryptionKey: hex(loadKey(space.path, 'encryption_key'))
  })
})
