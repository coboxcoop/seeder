const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/seeder')
const Repository = require('@coboxcoop/repository')
const Replicator = require('../app/models/replicator')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, 'replicators')
  const createReplicator = Replicator(namespace, opts.identity, opts)
  const repository = Repository(namespace, createReplicator)
  if (isDevelopment()) repository.on('entry', logResource)
  return repository
}
