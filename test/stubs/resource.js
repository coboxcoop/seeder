const crypto = require('@coboxcoop/crypto')
const sinon = require('sinon')
const { hex } = require('../../util')

module.exports = function stubResource (stubs) {
  return function Space (storage, key, opts = {}) {
    return {
      name: opts.name,
      address: hex(key),
      path: storage,
      bytesUsed: sinon.stub(),
      ...stubs
    }
  }
}
