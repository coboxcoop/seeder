#!/usr/bin/env node

const debug = require('@coboxcoop/logger')('@coboxcoop/seeder:cli')
const yargs = require('yargs')
const path = require('path')
const os = require('os')

const usage = `CoBox Hub 1.0 - a tool for distributed and mutual backup infrastructure

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: $0 <command> [options]`

const epilogue = `For more information on cobox read the manual: man cobox-seeder
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.`

if (require.main === module) return require('@coboxcoop/seeder-cli')
  .usage(usage)
  .option('config', { describe: 'path to .coboxseederrc file', default: path.join(os.homedir(), '.coboxseederrc') })
  .middleware((argv) => Object.assign(argv, config.load(argv.config)))
  .commandDir('bin')
  .demandCommand()
  .alias('h', 'help')
  .alias('v', 'version')
  .help()
  .epilogue(epilogue)
  .argv

exports.command = '$0 <command>'
exports.describe = 'run your own cobox backup node'
exports.handler = () => {}
exports.builder = (yargs) => {
  return yargs
    .commandDir('bin')
    .demandCommand()
    .alias('h', 'help')
    .alias('v', 'version')
    .help()
}
